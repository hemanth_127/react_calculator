import { useState } from 'react'
import './App.css'

function App () {
  const [current, setNext] = useState('')
  const [prevres, res] = useState(0)

  function back () {
    if (current.length <= 1) return ''
    else {
      return setNext(current.slice(0, -1))
    }
  }

  function backChecker (e) {
    console.log(e.target.textContent)
    const l = ['+', '-', '*', '/']
    if (l.includes(current.slice(-1))) {
      return setNext(current.slice(0, -1) + e.target.textContent.trim())
    } else {
      return setNext(current + e.target.textContent.trim())
    }
  }

  // console.log(current)
  return (
    <>
      <div className='container'>
        <input
          type='text'
          readOnly
          value={current}
          // onChange={e => setNext(e.target.value)}
        />
      </div>
      <button className='btn' onClick={() => res(eval(current))}>
        calculate
      </button>
      <p>result value:{prevres}</p>
      <div className='buttons'>
        <button onClick={() => setNext('')}>AC</button>
        <button onClick={back}>back</button>
        <button onClick={() => setNext(current + '0')}>0</button>
        <button onClick={backChecker}>/</button>
        
      </div>
      <div className='buttons'>
        <button onClick={() => setNext(current + '7')}>7</button>
        <button onClick={() => setNext(current + '8')}>8</button>
        <button onClick={() => setNext(current + '9')}>9</button>
        <button onClick={backChecker}> * </button>
      </div>
      <div className='buttons'>
        <button onClick={() => setNext(current + '4')}>4</button>
        <button onClick={() => setNext(current + '5')}>5</button>
        <button onClick={() => setNext(current + '6')}>6</button>
        <button onClick={backChecker}>-</button>


      </div>
      <div className='buttons'>
        <button onClick={() => setNext(current + '1')}>1</button>
        <button onClick={() => setNext(current + '2')}>2</button>
        <button onClick={() => setNext(current + '3')}>3</button>
        <button onClick={backChecker}>+</button>


       
      </div>
    </>
  )
}

export default App
